/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Bottom from './src/BottomTab.tsx'
import StackScreen from './src/Stack.tsx'
import ListHeader from './src/Screen/List/ListHeader'
import Count from './src/count'

AppRegistry.registerComponent(appName, () => StackScreen );
