import * as React from 'react';
import { Button, View, Text, Image, ImageBackground, StyleSheet } from 'react-native';
import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createStackNavigator,StackScreenProps } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Bottom from './BottomTab';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function Screen1({navigation}){
    return(
        <View>
            <Text>11111</Text>
            <Button title="next" onPress={()=> navigation.navigate("Screen2")}></Button>
        </View>
    )
}
function Screen2({navigation}){
    return(
        <View>
            <Text>22222</Text>
            <Button title="next" onPress={()=> navigation.navigate("Bottom")}></Button>
        </View>
    )
}

function StackScreen (){
    
    return(
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Screen1" headerMode="none">
                <Stack.Screen name="Bottom" component={Bottom}></Stack.Screen>
                <Stack.Screen name="Screen1" component={Screen1}></Stack.Screen>
                <Stack.Screen name="Screen2" component={Screen2}></Stack.Screen>
            </Stack.Navigator>
        </NavigationContainer>
    )
}
export default StackScreen;