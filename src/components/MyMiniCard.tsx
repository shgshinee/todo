import  React, {useState} from 'react';
import { Button, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

    const data = [
        {title: 'Money', date:'2021-06-10', category: 'work'},
    ];

const MyMiniCard = () => {
    const [show, setShow ] = useState(false);
    return (
        <TouchableOpacity style={styles.miniCard}>
            <View>
                <Text> title </Text>
            </View>
            <View>
                <AntDesign name={show?"star":"staro"} color="#778899" size={30} onPress={() => setShow(!show)}/>
            </View>
        </TouchableOpacity>
    )
}

export default MyMiniCard

const styles = StyleSheet.create({
    miniCard: {
    margin:"5%",
    padding: "4%",
    width: "90%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#D3D3D3",
    borderRadius: 10,
    }
  });
 




