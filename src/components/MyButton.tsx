import  React, {useState} from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MyInputText from './MyInputText';

const MyButton = () => {
    const [input, setInput] = useState(true);
    return (
        <View>
            {!input && <MyInputText/>}
        <TouchableOpacity style={styles.add} onPress={() => setInput(!input)} >
            <AntDesign name="plus" size={20} color="#FFFFFF"/>
        </TouchableOpacity>
        </View>

    );
}

export default MyButton

const styles = StyleSheet.create({
    add: {
        marginTop: 10,
        marginLeft: '80%',
        marginBottom: 10,
        paddingTop: 15,
        paddingBottom: 15,
        width: 50,
        alignItems: 'center',
        backgroundColor: '#2196F3',
        borderRadius: 100,
      },
      addText: {
        textAlign: 'center',
        padding: 20,
        color: 'white'
      },
})
