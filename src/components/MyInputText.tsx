import React from 'react';
import { Button, View, Text, Image, ImageBackground, StyleSheet } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import firestore from '@react-native-firebase/firestore';

const usersCollection = firestore().collection('Users');
// db ruu nemeh 
const addFb = () => {
    firestore ()
    .collection('Users')
    .add({
        name: 'Ada Lovelace',
        age: 30,
    })
    .then(() => {
        console.log('User added!');
    });
}

function MyInputText(){
  
    return (
        <View style={styles.inputText}>
            <SafeAreaView>
                <TextInput
                style={styles.input}
                placeholder="input new Task"
                placeholderTextColor="#778899"
                />
                <View style={styles.addCat}>
                    <Button title="Category" onPress={addFb} color="#778899"/>
                    <Button title="Day" onPress={addFb} color="#778899"/>
                    <Button title="nemeh" onPress={addFb} color="#778899"/>
                </View>
            </SafeAreaView>
            
        </View>
    );
    };
  
    const styles = StyleSheet.create({
        input: {
            width: "90%",
            height: 50,
            alignItems: 'center',
            justifyContent: "space-between",
            fontSize: 20,
            margin: 12,
            paddingLeft: 20,
            color: 'black',
            borderWidth: 1,
            borderRadius: 100,
            borderColor:'#778899',
        },
        inputText: {
            backgroundColor: '#D3D3D3',
            borderRadius: 10,
            alignItems: 'center',
            marginTop:"5%",
            padding: "4%",
            width: "90%",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
        },
        addCat: {
            width: "90%",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
        },
    });

export default MyInputText;
