import  React, {useState} from 'react';
import { Button, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

    const data = [
        {title: 'Money', date:'2021-06-10', category: 'work'},
    ];

const MyCard = () => {
    const [show, setShow ] = useState(false);
    const [check, setCheck] = useState(false);
    return (
            <TouchableOpacity  style={styles.CardCont}>
                <View>
                    <MaterialCommunityIcons name={check?"checkbox-marked-circle":"checkbox-blank-circle-outline"} color="#778899" size={30} onPress={() => setCheck(!check)}/>
                </View>
                <View>
                    <Text> title </Text>
                    <Text> Date</Text>
                </View>
                <View>
                    <AntDesign name={show?"star":"staro"} color="#778899" size={30} onPress={() => setShow(!show)}/>
                </View>
            </TouchableOpacity>
    )
}

export default MyCard

const styles = StyleSheet.create({
    CardCont: {
        marginTop:"5%",
        padding: "4%",
        width: "90%",
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        backgroundColor: "#D3D3D3",
        borderRadius: 10,
}
})




