import  React, {useState} from 'react';
import { Button, View, Text, Image, ImageBackground, StyleSheet, Alert, TouchableOpacity } from 'react-native';
// import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createStackNavigator,StackScreenProps } from '@react-navigation/stack';
import All1 from './All';
import List1 from './List';
import MyButton from '../../components/MyButton';
import MyCard from '../../components/MyCard';
import MyInputText from '../../components/MyInputText';
import { firebase } from '@react-native-firebase/firestore';


const Stack = createStackNavigator();
const TopTab = createMaterialTopTabNavigator();

const data = [
    {title: 'Money', date:'2021-06-10', category: 'work'},
];



function All({}){
    const handler =()=>{
        console.log("hh");
    }
    return(
        <View style={{alignItems:"center"}}>
            <MyCard/>
            <MyButton/>
        </View>
    )
}                                                                                                           
const styles = StyleSheet.create({
    container: {
      paddingTop: 60,
      alignItems: 'center',
    },
   CardCont: {
       padding: "4%",
       width: "90%",
       display: "flex",
       flexDirection: "row",
       justifyContent: "space-between",
       backgroundColor: "#D3D3D3",
       borderRadius: 10,
   }
  });

function Work(){
    return(
        <View style={{alignItems:"center"}}>
            <MyCard/>
            <MyButton/>
        </View>
    )
}

function Personal(){
    return(
        <View style={{alignItems:"center"}}>
            <MyCard/>
            <MyButton/>
        </View>
    )
}

function Wishlist(){
    return(
        <View style={{alignItems:"center"}}>
            <MyCard/>
            <MyButton/>
        </View>
    )
}

function ListHeader() {
  return (
    <TopTab.Navigator>
        <TopTab.Screen name="All" component={All}></TopTab.Screen>
        <TopTab.Screen name="Work" component={Work}></TopTab.Screen>
        <TopTab.Screen name="Personal" component={Personal}></TopTab.Screen>
        <TopTab.Screen name="Wishlist" component={Wishlist}></TopTab.Screen>
    </TopTab.Navigator>
  );
}


export default ListHeader;