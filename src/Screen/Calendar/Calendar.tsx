import  React, {useState} from 'react';
import { Button, View, Text, Image, ImageBackground, StyleSheet } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import MyMiniCard from '../../components/MyMiniCard';

function Calendar1(){
    return (
        <View>
            <SafeAreaView>
                <Calendar/>
            </SafeAreaView>
            <MyMiniCard/>
        </View>
        )
}



export default Calendar1;