import * as React from 'react';
import { Button, View, Text, Image, ImageBackground, StyleSheet } from 'react-native';
import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createStackNavigator,StackScreenProps } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import List1 from './Screen/List/List';
import Calendar1 from './Screen/Calendar/Calendar';
import Overview1 from './Screen/Overview/Overview';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
function List(){
    return(
        <List1></List1>
    )
}

function Calendar(){
    return(
        <View>
            <Calendar1></Calendar1>
        </View>
    )
}

function Bottom (){

    return(
        <Tab.Navigator>
            <Tab.Screen name="List" component={List}></Tab.Screen>
            <Tab.Screen name="Calendar" component={Calendar}></Tab.Screen>
    </Tab.Navigator> 
    )
}
export default Bottom;